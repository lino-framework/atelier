==============================
Atelier, rstgen and sphinxfeed
==============================

.. _atelier:

Atelier
=======

.. py2rst::

  import atelier
  print(atelier.SETUP_INFO['long_description'])



.. toctree::
   :maxdepth: 1

   usage
   config

Using rstgen
============

.. toctree::
   :maxdepth: 1

   rstgen
   sphinxext/index

Maintenance
===========

.. toctree::
   :maxdepth: 1

   api/index
   changes/index



.. toctree::
   :hidden:

   invlib
   sheller
