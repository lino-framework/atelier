=======================
The ``atelier`` package
=======================




`atelier` is a collection of tools for maintaining your Python software
repositories using a library of `invoke <https://www.pyinvoke.org/>`_ commands.

- Source code: https://gitlab.com/lino-framework/atelier
- Documentation: https://lino-framework.gitlab.io/atelier


